# AMQP MESSAGE PRODUCER WRAPPER

AMQP producer wrapper is a library that uses [`com.rabbitmq.amqp-client` version `5.7.3`](https://mvnrepository.com/artifact/com.rabbitmq/amqp-client/5.7.3) and adds a layer for easier connectivity with the PrEsto Communication and Messaging Broker.

Libraries are available on Nissatech public Maven, and in order to use them you can:

- #### Add the repository and dependency tag to your pom.xml file:
```xml
<repository>
    <id>archiva.public</id>
    <name>Nissatech Public Repository</name>
    <url>https://maven.nissatech.com/repository/public/</url>
    <releases>
        <enabled>true</enabled>
    </releases>
    <snapshots>
        <enabled>false</enabled>
    </snapshots>
</repository>
```
```xml
<dependency>
    <groupId>com.nissatech.presto</groupId>
    <artifactId>amqp-producer</artifactId>
    <version>4.0</version>
</dependency>
```
- #### Download jar and include it in your project:

https://maven.nissatech.com/repository/public/com/nissatech/presto/amqp-producer/4.0/amqp-producer-4.0-jar-with-dependencies.jar

## Basic usage
```java
import producer.AmqpProducer;
import exceptions.NoHostException;
import java.io.IOException;

public class Send_AMQP {
    public static void main(String argv[]) {
        String topic = "amqp.sending.test";
        AmqpProducer producer = new AmqpProducer("3.120.91.124", true, topic);
        //producer.setUsernameAndPassword("nissatech","**********");
        //producer.useSSL("3.120.91.124", "**********");
        int i = 0;
        try {
            while (true) {
                String Object = "Hello world!";
                producer.publish(Object);
                Thread.sleep(10000);
            }
        } catch (InterruptedException | NoHostException | IOException e1) {
            e1.printStackTrace();
        }
    }
}
```
## Constructors

`AmqpProducer(String host_or_dns_address, boolean is_DNS_address, String topic)` - creates instance of AmqpProducer

| Param | Description | Required |
|---|:---:|---|
| host_or_dns_address | IP address of broker node or of the load balancing DNS | yes |
| is_DNS_address | is the first field DNS address or not | yes |
| topic | topic name to which message should be sent to | no |

> Note: There are two constructors. One with all 3 arguments and other with only first 2, so when publishing message you need to use method with newTopic argument.

By default:
- [Automatic recovery](https://www.rabbitmq.com/api-guide.html#connection-recovery) and [topology recovery](https://www.rabbitmq.com/api-guide.html#topology-recovery) are disabled because we create a new connection
and channel per message. Also, connection timeout (connection establishment timeout in
milliseconds; zero for infinite) is set to 0.
- Port is automatically set to 5672. When the user enables SSL/TLS on the initiation of a connection, the port is changed to 5671.

## Setters

`void setUsernameAndPassword(String username, String password)` - set username and password for connecting to the broker

`void setVhost(String vhost)` - set [virtual host](https://www.rabbitmq.com/vhosts.html) for connecting to the broker

`void useSSL(String vault, String token)` - set vault address and token for SSL/TLS connection
- sslSockerFactory is prepared with the certificate
  received from the Vault server (TLS 1.2) and added to the connection factory. If it failed to create this
  factory it logs a warning and set connection without SSL.

`void turnOnSsl(boolean ssl)` - turn on/off usage of SSL/TLS certificates

`void setPersistent(boolean persistent)` - turn on/off persistence of the messages

`void setBufferedMessages(List<BufferedMessage> bufferedMessages)` - add messages to the buffer (used to restore state after producer app goes down)

`void setNewExchange(String exchangeName, BuiltinExchangeType exchangeType)` - set new exchange name and type

`void setNewTopicExchange(String exchangeName)` - set new topic exchange name

## Getters

`String getUsername()` - get current username

`String getPassword()` - get current password
- for now, it uses the default (guest) username and password.

`String getVhost()` - get current virtual host
- default vhost is "/", and probably there is no need for some other, but we enable the user to
  set vhost if later we decide to create specific vhost for specific users.
  
`boolean isSsl()` - check if connection is secure

`boolean arePersistent()` - check if all messages are persistent

`List<BufferedMessage> getBufferedMessages()` - get all buffered messages
-  user can save all buffered messages to file if the producer app goes down

`String getExchangeName()` - get set exchange name

`BuiltinExchangeType getExchangeType()` - get exchange type

## Publish methods

`void publish(String payload, String newTopic, boolean persistent, boolean retained)` - publish message to the broker

| Param | Description | Required | Default
|---|:---:|---|---|
| payload | body of message to be sent | yes | / |
| newTopic | topic name on which message should be sent | no | topic set in constructor |
| persistent | QOS of message | no | true |
| retained | deprecated | no | false |

> Note: There are 4 versions of this method with a different number of arguments

Publish method checks if the topic is not null, buffers message, tries to create connection and channel,
renews the certificate if needed, adds listeners, declares exchange, publishes message to a defined topic,
logs work and closes channel and connection:
- Check if the topic name that user tries to connect isn’t null. Log error message and return
IOException.
- Persistence of message is automatically set to true. With one of the publish functions, the
user can set persistence of the current message. Or it can set persistence for all messages
with setPersistent. True set Delivery mode (Should the message be persisted to [disk](https://www.rabbitmq.com/persistence-conf.html)?) to 2,
and false is setting it to 1.
- Buffer message and log work.
- After this try to create connection and channel. If the connection fails because of an expired
certificate, the certificate is renewed and new connection created. If it fails for some other
reason, log the message and error.
- By declaring exchange we verify that exchange exists, or create it if needed. This method
creates an exchange if it does not already exist, and if the exchange exists, verifies that it is
of the correct and expected type. Default exchange is presto.cloud TOPIC exchange, and
probably there is no need for some other, but we enable the user to setNewExchange with
other name and type of exchange.
- On the first successful connection, publish all buffered messages like FIFO, and log work.
- If some exception occurred in these steps, connections will be closed. The error will be
logged. Because messages are buffered we don’t need to send the exception to the user.
- In the end, the channel and connection are closed.