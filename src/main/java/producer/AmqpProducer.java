package producer;

import com.rabbitmq.client.*;
import exceptions.NoHostException;

import javax.net.ssl.SSLHandshakeException;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

public class AmqpProducer extends RabbitMqProducer {

    private String exchangeName = "presto.cloud"; //"amq.topic";
    private BuiltinExchangeType exchangeType = BuiltinExchangeType.TOPIC;
    private ConnectionFactory connectionFactory;
    private Address[] addresses;

    public AmqpProducer(String address, boolean isDNS, String topic) {
        super(address, isDNS, topic);
    }

    public AmqpProducer(String address, boolean isDNS) {
        super(address, isDNS, null);
    }

    public void setNewTopicExchange(String exchangeName) {
        setNewExchange(exchangeName, BuiltinExchangeType.TOPIC);
    }

    public void setNewExchange(String exchangeName, BuiltinExchangeType exchangeType) {
        this.exchangeName = exchangeName;
        this.exchangeType = exchangeType;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public BuiltinExchangeType getExchangeType() {
        return exchangeType;
    }

    @Override
    protected void setUpConnection() {
        connectionFactory = new ConnectionFactory();

        connectionFactory.setVirtualHost(vhost);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);

        connectionFactory.setAutomaticRecoveryEnabled(false);
        connectionFactory.setTopologyRecoveryEnabled(false);
        connectionFactory.setConnectionTimeout(0);

        amqpTrySSL();
    }

    private void amqpTrySSL() {
        protocolPart = ":5672";
        if (isSsl()) {
            try {
                prepareSSL();
                protocolPart = ":5671";
                connectionFactory.setSocketFactory(sslSocketFactory);
            } catch (Exception e) {
                logger.warn("[x] Connection without SSL!");
                loggException(e);
            }
        }

        updateAllIps();
    }

    @Override
    protected void updateAllIps() {
        addresses = null;
        if (dns != null) {
            if (!ipList.isEmpty()) {
                String allIps = ipList.stream().map(i -> i + protocolPart).collect(Collectors.joining(","));
                addresses = Address.parseAddresses(allIps);
            }
        } else if (host != null) {
            addresses = new Address[]{Address.parseAddress(host + protocolPart)};
        }
    }

    private void tearDownConnection(Channel channel, Connection connection) {
        try {
            if (channel != null) {
                if (channel.isOpen())
                    channel.close();
                channel.abort();
            }
        } catch (IOException | TimeoutException | ShutdownSignalException e) {
            logger.error("[x] Couldn't close channel!");
            loggException(e);
        }

        try {
            if (connection != null) {
                if (connection.isOpen())
                    connection.close();
                connection.abort();
            }
        } catch (IOException | ShutdownSignalException e) {
            logger.error("[x] Couldn't close connection!");
            loggException(e);
        }
    }

    @Override
    protected void publish(String payload, String newTopic, boolean persistent, boolean retained) throws NoHostException, IOException {
        if (newTopic == null) {
            String errorMessage = "[x] Topic name can't be null";
            logger.error(errorMessage);
            throw new IOException(errorMessage);
        }

        int deliveryMode = 1;
        if (persistent)
            deliveryMode = 2;

        bufferedMessages.add(new BufferedMessage(payload, newTopic, deliveryMode, retained));
        logger.info("[x] Buffered message!");

        Connection connection;
        Channel channel;
        try {
            connection = getConnection();
        } catch (TimeoutException | IOException e) {
            logger.error("[x] Couldn't establish connection!");
            if (e instanceof SSLHandshakeException && e.getMessage().contains("certificate_expired")) {
                amqpTrySSL();
                try {
                    connection = getConnection();
                } catch (TimeoutException | IOException e1) {
                    loggException(e1);
                    return;
                }
            } else {
                loggException(e);
                return;
            }
        }

        try {
            channel = connection.createChannel();
        } catch (IOException e) {
            logger.error("[x] Couldn't create channel!");
            loggException(e);
            tearDownConnection(null, connection);
            return;
        }

        try {
            channel.exchangeDeclare(exchangeName, exchangeType.getType(), true);
            synchronized (bufferedMessages) {
                Iterator<BufferedMessage> iterator = bufferedMessages.iterator();
                while (iterator.hasNext()) {
                    BufferedMessage bufferedMessage = iterator.next();
                    publishMessage(channel, bufferedMessage);
                    logger.info("[x] Published \"{}\" message {} on {}.\n", bufferedMessage.getTopic(), bufferedMessage.getPayload(), channel.getConnection().getAddress().getHostAddress() + ":" + channel.getConnection().getPort());
                    iterator.remove();
                }
            }
        } catch (IOException e) {
            logger.error("[x] Some messages are not published, yet!");
            loggException(e);
        } finally {
            tearDownConnection(channel, connection);
        }
    }

    private Connection getConnection() throws IOException, TimeoutException, NoHostException {
        Connection connection;
        if (addresses != null)
            connection = connectionFactory.newConnection(addresses);
        else {
            String errorMessage = "[x] No host provided or DNS didn't return any record!";
            logger.error(errorMessage);
            throw new NoHostException(errorMessage);
        }
        return connection;
    }

    private void publishMessage(Channel channel, BufferedMessage bufferedMessage) throws IOException {
        AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder().deliveryMode(bufferedMessage.getQos()).build();
        channel.basicPublish(exchangeName, bufferedMessage.getTopic(), properties, bufferedMessage.getPayload().getBytes());
    }

//    public static void main(String[] args) {
//        AmqpProducer producer = new AmqpProducer("3.120.91.124", false, "test.1.2.3.4.5");
//        producer.useSSL("3.120.91.124", "s.pV8wvr4dkuq5ii476ddErT07");
//
//        for (int i = 1; ; i++)
//            try {
//                producer.publish(i + "tset");
//                Thread.sleep(5000);
//            } catch (IOException | InterruptedException | NoHostException e) {
//                e.printStackTrace();
//            }
//    }
}
